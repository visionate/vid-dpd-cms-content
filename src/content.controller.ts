import { BeCLog, BeCLogger, ContentModel, DynContentModel, I18nModel, PageModel, TemplateModel } from '@vid/core';
import { get, keys, nth, set, split, startsWith, unset } from 'lodash';

export type ValueKey = { value: any, contentKey: string };

export class ContentController {

  public logger: BeCLogger = BeCLog.createClogger( 'vid:cms-content-content', 'ContentController' );

  public dpd: any;

  public lang: string;

  public visitorID: string;

  public visitorIsSonepar: boolean;

  public projectID: string;

  public pageID: string;

  public contentID: string;

  public firstQuestion: string;

  public questionID: string;

  public answerID: string;

  constructor( dpd ) {
    this.dpd = dpd;
  }

  static mapImages( content: any, searchPath: string[], replacePath: string[] ) {
    const isBlank = function ( value ) {
      return typeof value === 'string' && value.length === 0;
    };
    const item = get( content, searchPath );
    if ( typeof item !== 'undefined' ) {
      if ( isBlank( item ) ) {
        unset( content, replacePath );
      } else {
        set( content, replacePath, item );
      }
    }
  }

  static mapHTML = ( val: any ) => {
    if ( val && typeof val === 'string' ) {
      return val.split( '<p>' )
                .join( '' )
                .split( '</p>' )
                .join( '<br/>' )
                .split( 'strong>' )
                .join( 'b>' );
    } else {
      return val;
    }
  };

  public handleContext( ctx ) {

    if ( ctx.query.lang ) {
      this.lang = ctx.query.lang;
    }

    if ( ctx.query.visitorID ) {
      this.visitorID = ctx.query.visitorID;
    }

    if ( ctx.query.projectID ) {
      this.projectID = ctx.query.projectID;
    }

    if ( ctx.query.pageID ) {
      this.pageID = ctx.query.pageID;
    }

    if ( ctx.query.contentID ) {
      this.contentID = ctx.query.contentID;
    }

    if ( ctx.query.firstQuestion ) {
      this.firstQuestion = '4ca72c79482e996e';  // hardcoded erste Frage
    }

    if ( ctx.query.questionID ) {
      this.questionID = ctx.query.questionID;
    }

    if ( ctx.query.answerID ) {
      this.answerID = ctx.query.answerID;
    }

    if ( this.projectID && this.lang ) {
      this.getProject( this.projectID )
          .then( ( result ) => ctx.done( null, result ) )
          .catch( e => ctx.done( e ) );
    } else if ( this.pageID && this.lang ) {
      this.getPage( this.pageID )
          .then( ( result ) => ctx.done( null, result ) )
          .catch( e => ctx.done( e ) );
    } else if ( this.contentID && this.lang ) {
      this.getContent( this.contentID )
          .then( ( result ) => ctx.done( null, result ) )
          .catch( e => ctx.done( e ) );
    } else if ( this.visitorID ) {
      this.getVisitorIsSonepar()
          .then( ( res: boolean ) => {
            this.visitorIsSonepar = res;
            // this.logger.info(  'visitorIsSonepar => ' + this.visitorIsSonepar );
            this.processQuestionsRequest( ctx );
          } )
          .catch( e => {
            this.logger.error( 'visitorIsSonepar => ' + e );
            this.processQuestionsRequest( ctx );
          } );
    } else {
      ctx.done( null, {
        error: 'not enough parameters!',
        contentID: 'please provide ?contentID=[12345678]&lang=[lang]',
        firstQuestion: 'please provide ?visitorID=[12345678]&firstQuestion=true&lang=[lang]',
        questionID: 'please provide ?visitorID=[12345678]&questionID=[12345678]&lang=[lang]',
        answerID: 'please provide ?visitorID=[12345678]&answerID=[12345678]&lang=[lang]'
      } );
    }
  }

  public processQuestionsRequest( ctx ) {
    if ( this.firstQuestion && this.lang ) {
      this.logger.info( 'processQuestionsRequest => visitorID: ' + this.visitorID + ' firstQuestion:' + this.firstQuestion );
      this.getQuestion( this.firstQuestion )
          .then( ( result ) => {
            this.updateVisitor( result, null, null, null )
                .then( () => ctx.done( null, result ) )
                .catch( e => ctx.done( null, result ) );
          } )
          .catch( e => ctx.done( e ) );

    } else if ( this.questionID && this.lang ) {
      this.logger.info( 'processQuestionsRequest => visitorID: ' + this.visitorID + ' questionID:' + this.questionID );
      this.getQuestion( this.questionID )
          .then( ( result ) => {
            this.updateVisitor( result, null, null, null )
                .then( () => ctx.done( null, result ) )
                .catch( e => ctx.done( null, result ) );
          } )
          .catch( e => ctx.done( e ) );

    } else if ( this.answerID && this.lang ) {
      this.logger.info( 'processQuestionsRequest => visitorID: ' + this.visitorID + ' answerID:' + this.answerID );
      const getQuestionByAnswerID = () => {
        this.getQuestionByAnswerID( this.answerID )
            .then( ( result ) => {
              if ( typeof result !== 'undefined' && result !== null && typeof result.result !== 'undefined' && result.result
                   === true ) {
                this.updateVisitor( null, null, parseInt( result.resultindex ), result.resultlabel )
                    .then( () => ctx.done( null, result ) )
                    .catch( e => ctx.done( null, result ) );
              } else {
                this.updateVisitor( result, null, null, null )
                    .then( () => ctx.done( null, result ) )
                    .catch( e => ctx.done( null, result ) );
              }
            } )
            .catch( e => ctx.done( e ) );
      };

      this.updateVisitor( null, this.answerID, null, null )
          .then( () => getQuestionByAnswerID() )
          .catch( ( e ) => {
            this.logger.error( 'processQuestionsRequest updateVisitor error => ' + e );
            getQuestionByAnswerID();
          } );
    }
  }

  // https://sonepar-il.dyndns.org:2504/sonepar/content?visitorID=a795787be421d8a6&firstQuestion=true&lang=de
  public getVisitorIsSonepar(): Promise<boolean> {
    return new Promise( ( resolve, reject ) => {
      this.dpd.soneparvisitors.get( {
                                      id: this.visitorID,
                                      $fields: {
                                        isSonepar: 1
                                      }
                                    }, ( visitor, error ) => {
        if ( error ) {
          this.logger.error( 'getVisitorIsSonepar => visitorID: ' + this.visitorID + ' error:' + error );
          reject( 'get => getVisitorIsSonepar:' + error );
        } else {
          // const visitor = visitors[ 0 ];
          resolve( visitor.isSonepar );
        }
      } );
    } );
  }

  public updateVisitor( question: any, answerID: any, resultindex: number, resultlabel: string ): Promise<any> {
    return new Promise( ( resolve, reject ) => {

      let questionID;
      if ( question ) {
        questionID = question.id;
      }

      this.logger.trace(
        ' => updateVisitor "' + this.visitorID + '" questionID "' + questionID + '", answerID "' + answerID + '", resultindex "'
        + resultindex + '", resultlabel "' + resultlabel + '" ' );

      if ( typeof question !== 'undefined' && question !== null ) {

        const getQuestionsQuery = {
          $fields: {
            'questions': 1,
            'answers': 1
          }
        };
        this.dpd.soneparvisitors.get( this.visitorID, getQuestionsQuery, ( questionsResult, getQuestionsError ) => {
          if ( getQuestionsError ) {
            this.logger.error( 'getQuestionsError visitorID: "' + this.visitorID + '" ', getQuestionsError );
            reject( 'updateVisitor => getQuestionsError:' + getQuestionsError );
          } else {
            let questions = [];
            if ( questionsResult && questionsResult.questions && Array.isArray( questionsResult.questions ) ) {
              // this.logger.info( 'questions visitorID: "'+this.visitorID+'" ', questionsResult.questions );
              // check if question is already there
              questions = questionsResult.questions.filter( item => item.id !== question.id );
            }
            questions.push( {
                              'id': question.id,
                              'questionlabel': question.questionlabel
                            } );

            // this.logger.info( 'questions visitorID: "'+this.visitorID+'" ', questions );
            const putQuestionQuery = {
              'questions': questions,
              'answersCache': question.answers
            };
            this.dpd.soneparvisitors.put( this.visitorID, putQuestionQuery, ( putQuestionResult, putQuestionError ) => {
              if ( putQuestionError ) {
                this.logger.error( 'updateVisitor => visitorID: ' + this.visitorID + ' putQuestionError:' + putQuestionError );
                reject( 'updateVisitor => putQuestionError:' + putQuestionError );
              } else {
                resolve( putQuestionResult );
              }
            } );
          }
        } );

      } else if ( typeof answerID !== 'undefined' && answerID !== null ) {
        this.logger.log( 'visitorID: "' + this.visitorID + '" if answerID', answerID );
        // get answer from answercache  by answerID first
        const getAnswersCacheQuery = {
          $fields: {
            'answersCache': 1,
            'questions': 1,
            'answers': 1
          }
        };
        this.dpd.soneparvisitors.get( this.visitorID, getAnswersCacheQuery, ( getAnswersCacheResult, getAnswersCacheError ) => {
          if ( getAnswersCacheError ) {
            this.logger.error(
              'updateVisitor => visitorID: ' + this.visitorID + ' getAnswersCacheError:' + getAnswersCacheError );
            reject( 'updateVisitor => getAnswersCacheError:' + getAnswersCacheError );
          } else {
            // this.logger.log( 'getAnswersCacheResult %o', getAnswersCacheResult );

            let answers = [];
            if ( getAnswersCacheResult && getAnswersCacheResult.answers && Array.isArray( getAnswersCacheResult.answers ) ) {
              // this.logger.info( 'getAnswersCacheResult.answers', getAnswersCacheResult.answers );
              // check if answer is already there
              answers = getAnswersCacheResult.answers.filter( item => item.id !== answerID );
            }
            // this.logger.info( 'getAnswersCacheResult.answersCache', getAnswersCacheResult.answersCache );
            const answersCache: any[] = getAnswersCacheResult.answersCache || [];
            const answer = answersCache.find( ( item ) => item.id === answerID );

            if ( answer ) {

              const questions: any[] = getAnswersCacheResult.questions || [];
              const question = questions.find( ( item ) => item.id === answer.parentquestionid );

              // check if question is already there
              answers = answers.filter( item => item.questionid !== question.id );

              answers.push( {
                              'questionid': question.id,
                              'questionlabel': question.questionlabel,
                              'id': answer.id,
                              'answerlabel': answer.answerlabel
                            } );
              const putAnswerQuery = {
                'answers': answers
              };
              // this.logger.log(  ' => if answerID %o', putAnswerQuery );
              this.dpd.soneparvisitors.put( this.visitorID, putAnswerQuery, ( putAnswerResult, putAnswerError ) => {
                if ( putAnswerError ) {
                  this.logger.error( 'updateVisitor => visitorID: ' + this.visitorID + ' putAnswerError:' + putAnswerError );
                  reject( 'updateVisitor => putAnswerError:' + putAnswerError );
                } else {
                  resolve( putAnswerResult );
                }
              } );
            } else {
              this.logger.warn( 'getAnswersCacheResult answer not found in cache %s', answerID );
              resolve( getAnswersCacheResult );
            }
          }
        } );
      } else if ( typeof resultlabel !== 'undefined' && resultlabel !== null ) {
        this.logger.log( 'visitorID: "' + this.visitorID + '" if resultlabel', resultlabel );
        const putTopicIndexQuery = {
          'topicIndex': resultindex,
          'topic': resultlabel,
          'answersCache': []
        };
        // this.logger.log(  ' => if resultindex putTopicIndexQuery %o', putTopicIndexQuery );

        this.dpd.soneparvisitors.put( this.visitorID, putTopicIndexQuery, ( putTopicIndexResult, putTopicIndexError ) => {
          if ( putTopicIndexError ) {
            this.logger.error( 'updateVisitor => visitorID: ' + this.visitorID + ' putTopicIndexError:' + putTopicIndexError );
            reject( 'updateVisitor => putTopicIndexError:' + putTopicIndexError );
          } else {
            resolve( putTopicIndexResult );
          }
        } );
      } else {
        this.logger.error( 'updateVisitor => visitorID: ' + this.visitorID + ' => noting to do' );
        reject( 'visitorID: "' + this.visitorID + '" updateVisitor => noting to do' );
      }
    } );
  }

  public getQuestion( questionID ): Promise<any> {
    return new Promise( ( resolve, reject ) => {
      let questionRes;
      this.getRefEntry( 'contents', questionID )
          .then( ( contentItem ) => {
            this.getQuestionRes( contentItem )
                .then( questionRes => resolve( questionRes ) );
          } );
    } );
  }

  public getQuestionByAnswerID( answerID ): Promise<any> {
    return new Promise( ( resolve, reject ) => {
      let questionRes;
      this.getRefByQuery( 'contents', {
            'content.parentanswerid': answerID,
            $sort: { 'sorting': 1 }
          } )
          .then( ( contentItems ) => {
            if ( contentItems[ 0 ] ) {
              this.getQuestionRes( contentItems[ 0 ] )
                  .then( questionRes => resolve( questionRes ) );
            } else {
              resolve( null );
            }
          } );
    } );
  }

  public getQuestionRes( contentItem ): Promise<any> {
    return new Promise( ( resolve ) => {
      let questionRes;

      questionRes = contentItem.content;
      questionRes[ 'id' ] = contentItem.id;
      if ( !questionRes.resultlabel ) {
        questionRes[ 'resultlabel' ] = '';
        questionRes[ 'result' ] = false;
      }
      questionRes[ 'icon' ] = questionRes.icon ? questionRes.icon.image : '';

      this.resolveReferences( questionRes, null )
          .then( ( resolveLangRefsRes: ValueKey ) => {
            const resVal = resolveLangRefsRes.value;
            keys( resVal )
              .forEach( ( key ) => set( questionRes, key, resVal[ key ] ) );

            // this.getRefByQuery('contents', {"content.parentquestionid": "c0a0ca035ac59b9d" })
            this.getRefByQuery( 'contents', {
                  'content.parentquestionid': contentItem.id,
                  $sort: { 'sorting': 1 }
                } )
                .then( ( getRefByQueryRes ) => {
                  const proms: Promise<ValueKey>[] = getRefByQueryRes.map(
                    ( cItem ) => this.resolveReferences( cItem.content, null ) );
                  Promise
                    .all( proms )
                    .then( ( contentsRes: ValueKey[] ) => {
                      questionRes.answers = contentsRes.map( ( res: ValueKey, index ) => {
                        const answerRes = res.value;
                        answerRes[ 'id' ] = getRefByQueryRes[ index ].id;
                        answerRes[ 'icon' ] = answerRes.icon ? answerRes.icon.image : '';
                        return answerRes;
                      } );
                      // filter isSonepar
                      if ( typeof this.visitorIsSonepar === 'undefined' || this.visitorIsSonepar === false ) {
                        questionRes.answers = questionRes.answers.filter( ( answer ) => {
                          return !(typeof answer.isSonepar !== 'undefined' && answer.isSonepar !== null && answer.isSonepar
                                   === true);
                        } );
                      }
                      // // this.logger.log(   ' => pageReturn', pageReturn );
                      resolve( questionRes );
                    } );
                } );
          } );

    } );
  }

  protected getPage( pageID: string ): Promise<any> {
    return new Promise( ( resolve, reject ) => {
      // noinspection JSDeprecatedSymbols
      this.dpd.pages.get( pageID, ( page, err ) => {
        if ( err ) {
          reject( err );
        } else {
          if ( page ) {
            this.getPageContents( page )
                .then( getPagesContentsResult => {
                  this.getChildPages( pageID )
                      .then( ( result ) => {
                        if ( result && Array.isArray( result ) && result.length > 0 ) {
                          getPagesContentsResult.children = result;
                        } else {
                          delete getPagesContentsResult.children;
                        }
                        resolve( getPagesContentsResult );
                      } )
                      .catch( e => reject( 'getPage:' + e ) );
                } )
                .catch( e => reject( 'getPage:' + e ) );
          } else {
            reject( 'no page found' );
          }
        }
      } );
    } );
  }

  protected getProject( projectID: string ): Promise<any> {
    return new Promise( ( resolve, reject ) => {

      const pagesQuery = {
        'projectUid': projectID,
        'parentID': '',
        $sort: { 'sorting': 1 }
      };

      // noinspection JSDeprecatedSymbols
      this.dpd.pages.get( pagesQuery, ( pages, err ) => {
        if ( err ) {
          reject( err );
        } else {
          if ( pages && pages [ 0 ] ) {
            const rootPage = pages[ 0 ];
            this.getPageContents( rootPage )
                .then( rootPageresult => {
                  this.getChildPages( rootPage.id )
                      .then( ( result ) => {
                        if ( result && Array.isArray( result ) && result.length > 0 ) {
                          rootPageresult.children = result;
                        } else {
                          delete rootPageresult.children;
                        }
                        resolve( rootPageresult );
                      } );
                } )
                .catch( e => reject( e ) );
          } else {
            reject( 'no pages' );
          }
        }
      } );
    } );
  }

  protected getChildPages( parentID: string ): Promise<any> {
    return new Promise( ( resolve, reject ) => {

      const pagesQuery = {
        'parentID': parentID,
        'hidden': { '$ne': true },
        $sort: { 'sorting': 1 }
      };
      const returnResult = [];

      // noinspection JSDeprecatedSymbols
      this.dpd.pages.get( pagesQuery, ( pages, err ) => {
        if ( err ) {
          reject( err );
        } else {
          if ( pages ) {
            Promise
              .all( pages.map( page => {
                return new Promise( ( resolve, reject ) => {
                  this.getPageContents( page )
                      .then( ( pageContentsRes ) => {
                        this.getChildPages( page.id )
                            .then( ( childPagesResult ) => {
                              if ( childPagesResult && Array.isArray( childPagesResult ) && childPagesResult.length > 0 ) {
                                pageContentsRes.children = childPagesResult;
                              } else {
                                delete pageContentsRes.children;
                              }
                              resolve( pageContentsRes );
                            } )
                            .catch( e => reject( e ) );
                      } );
                } );
              } ) )
              .then( result => resolve( result ) )
              .catch( e => reject( e ) );
          } else {
            reject( 'no pages' );
          }
        }
      } );
    } );
  }

  protected getPageContents( page ): Promise<any> {
    return new Promise( ( resolve ) => {
      // this.logger.trace( 'getPageContents' );
      const pageReturn = {
        pageID: page.id,
        label: page.label,
        content: []
      };
      const queryContents = {
        'parentID': page.id,
        'hidden': { '$ne': true },
        $sort: { 'sorting': 1 }
      };
      // get all contentModels
      this.getRefByQuery( 'contents', queryContents )
          .then( ( contents: ContentModel[] ) => {
            // this.logger.log( 'getPageContents => contents', contents );

            const proms: Promise<ContentModel>[] = contents.map( ( contentItem ) => {
              return this.resolveAllRefs( contentItem );
            } );

            Promise
              .all( proms )
              .then( ( contentsRes: ContentModel[] ) => {
                contentsRes.forEach( ( content: ContentModel ) => {
                  if ( content.templateType === 'page' ) {
                    // schreibt alle Properties aus dem content in die Page
                    keys( content.content )
                      .forEach( ( key ) => {
                        if ( content.content[ key ] ) {
                          set( pageReturn, key, content.content[ key ] );
                        }
                      } );
                  } else {
                    pageReturn.content.push( content.content );
                  }
                } );

                if ( pageReturn.content.length === 0 ) {
                  delete pageReturn.content;
                }

                // this.logger.log( 'contentsRes', contentsRes );
                resolve( pageReturn );
              } );
          } )
          .catch( e => {
            this.logger.error( 'getPageContents => error => ' + JSON.stringify( e ) );
            resolve( pageReturn );
          } );
    } );
  }

  protected getContent( contentID: string ): Promise<DynContentModel> {
    return new Promise( ( resolve, reject ) => {
      this.logger.log( 'getContent contentID lang', contentID, this.lang );
      this.getRefEntry( 'contents', contentID )
          .then( ( contentItem: ContentModel ) => {
            this.resolveAllRefs( contentItem )
                .then( ( resolveRefscontent ) => {
                  contentItem.content = resolveRefscontent.content;
                  resolve( contentItem.content );
                } );
          } )
          .catch( e => {
            this.logger.error( 'getContent => error => ' + JSON.stringify( e ) );
            reject( e );
          } );
    } );
  }

  protected resolveAllRefs( contentItem: ContentModel ): Promise<ContentModel> {
    return new Promise( ( resolve, reject ) => {
      // this.logger.trace( 'resolveAllRefs' );

      this.resolveReferences( contentItem.content, null )
          .then( ( resolveReferencesRes: ValueKey ) => {

            const resVal = resolveReferencesRes.value;

            // set References values to contentItem.content
            keys( resVal )
              .forEach( ( key ) => set( contentItem.content, key, resVal[ key ] ) );

            if ( contentItem && contentItem.templateID ) {
              // get templateType
              this.getRefEntry( 'templates', contentItem.templateID )
                  .then( ( template: TemplateModel ) => {
                    contentItem.templateType = template.templateType;
                    // this.logger.log( ' => contentItem, template', contentItem, template );
                    resolve( contentItem );
                  } )
                  .catch( e => reject( e ) );
            } else {
              resolve( contentItem );
            }
          } );
    } );
  }

  protected resolveReferences( content: DynContentModel, contentKey: string ): Promise<ValueKey> {
    // return Promise.resolve(content);
    return new Promise( ( resolve ) => {
      //  // this.logger.log(   ' => resolveReferences content contentKey', content, contentKey );
      if ( content && typeof (content as any).hidden !== 'undefined' ) {
        if ( (content as any).hidden === true ) {
          return resolve( {
                            value: null,
                            contentKey: contentKey
                          } );
        } else {
          delete (content as any).hidden;
        }
      }

      ContentController.mapImages( content, [ 'icon', 'image' ], [ 'icon' ] );
      ContentController.mapImages( content, [ 'image', 'image' ], [ 'image' ] );
      ContentController.mapImages( content, [ 'Image', 'image' ], [ 'Image' ] );
      ContentController.mapImages( content, [ 'backgroundImage', 'image' ], [ 'backgroundImage' ] );
      ContentController.mapImages( content, [ 'video', 'video', 'file' ], [ 'video', 'video' ] );
      ContentController.mapImages( content, [ 'video', 'poster', 'image' ], [ 'video', 'poster' ] );
      ContentController.mapImages( content, [ 'icon', 'video', 'file' ], [ 'icon' ] );
      ContentController.mapImages( content, [ 'excelFile', 'file' ], [ 'excelFile' ] );

      unset( content, [ 'video', 'inputType' ] );

      let contentReturn;

      const proms: Promise<ValueKey>[] = keys( content )
        .map( ( key ) => this.analyseContentKeys( content[ key ], key ) );

      Promise
        .all( proms )
        .then( ( contentRes ) => {

          if ( Array.isArray( content ) ) {
            contentReturn = contentRes.map( content => content.value )
                                      .filter( ( val ) => typeof val !== 'undefined' && val !== null && val !== '' );
          } else {
            contentReturn = {};
            contentRes.forEach( content => {
              // // this.logger.log(   ' => set contentKey, value', content.contentKey, content.value );
              if ( typeof content.value !== 'undefined' && content.value !== null && content.value !== '' ) {
                set( contentReturn, content.contentKey, content.value );
              } else {
                unset( contentReturn, content.contentKey );
              }
            } );
          }

          resolve( {
                     value: contentReturn,
                     contentKey: contentKey
                   } );

        } );
    } );
  }

  protected analyseContentKeys( value: unknown, key ): Promise<ValueKey> {
    let valueReturn;
    if ( value ) {
      if ( Array.isArray( value ) ) {
        // this.logger.log( 'analyseContentKeys => value is Array', key, value );
        return new Promise<ValueKey>( ( resolve ) => {

          const valsNotNull = value.filter( val => val );

          const proms: Promise<ValueKey>[] = valsNotNull.map( arrValue => this.resolveReferences( arrValue, null ) );
          Promise.all( proms )
                 .then( arrRes => resolve( {
                                             value: arrRes.map( resItem => resItem.value )
                                                          .filter( val => val ),
                                             contentKey: key
                                           } ) );
        } );
      } else if ( typeof value !== 'string' && typeof value === 'object' ) {

        // this.logger.log( 'analyseContentKeys => value is object', key, value );
        return this.resolveReferences( value as DynContentModel, key );

      }
      {
        // // this.logger.log(   ' => value is String', value );
        if ( startsWith( value as string, 'ref:' ) ) {
          return new Promise<ValueKey>( ( resolve ) => {
            // // this.logger.log(   ' => value startsWith ref', value );
            const refArray = split( value as string, ':' );
            const refTable = nth( refArray, 1 );
            const refID = nth( refArray, 2 );

            switch ( refTable ) {

              case PageModel.dbPath:
                this.getPage( refID )
                    .then( pageRes => {
                      resolve( {
                                 value: pageRes,
                                 contentKey: key
                               } );
                    } );
                break;

              case ContentModel.dbPath:
                this.getContent( refID )
                    .then( contentRes => {
                      resolve( {
                                 value: contentRes,
                                 contentKey: key
                               } );
                    } );
                break;

              case I18nModel.dbPath:
                this.getRefEntry( refTable, refID )
                    .then( i18nRes => {
                      const retVal = get( i18nRes, [ 'values', this.lang ], '' );
                      resolve( {
                                 value: ContentController.mapHTML( retVal ),
                                 contentKey: key
                               } );
                    } );
                break;

              default:
                this.getRefEntry( refTable, refID )
                    .then( refRes => {
                      resolve( {
                                 value: ContentController.mapHTML( refRes ),
                                 contentKey: key
                               } );
                    } );
            }

          } );
        } else {
          // // this.logger.log(   ' => value NO ref', value );
          return Promise.resolve( {
                                    value: ContentController.mapHTML( value ),
                                    contentKey: key
                                  } );
        }
      }
    } else {
      // // this.logger.log(   ' => value NO ref', value );
      return Promise.resolve( {
                                value: ContentController.mapHTML( value ),
                                contentKey: key
                              } );
    }
  }

  protected getRefByQuery( refTable, query ): Promise<any[]> {
    return new Promise( resolve => {
      this.dpd[ refTable ].get( query, ( result, error ) => {
        if ( error ) {
          resolve( [] );
        } else {
          resolve( result );
        }
      } );
    } );
  }

  protected getRefEntry( refTable, refID ): Promise<any> {
    return new Promise( resolve => {
      this.logger.log( 'refTable refID', refTable, refID );
      this.dpd[ refTable ].get( refID, ( result, error ) => {
        if ( error ) {
          resolve( null );
        } else {
          resolve( result );
        }
      } );
    } );
  }
}
