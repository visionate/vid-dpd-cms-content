import { BeCLog, BeCLogger } from '@vid/core';
import * as internalClient from 'deployd/lib/internal-client.js';
import * as Resource from 'deployd/lib/resource.js';
import { ContentController } from './content.controller';

let env;
if ( (process as any).server && (process as any).server.options && (process as any).server.options.env ) {
  env = (process as any).server.options.env;
} else {
  env = null;
}

/**
 * Module setup.
 */
export class CmsContent extends Resource {

  public logger: BeCLogger = BeCLog.createClogger( 'vid:dpd-cms-content', 'index' );

  public static label = 'Cms-Content';

  public static events = [];

  public events;

  public static clientGeneration = true;

  public static basicDashboard = {
    settings: []
  };

  public clientGeneration = true;

  public config;

  public name;

  public session;

  public dpd: any;

  constructor( options ) {
    super( options );

    Resource.apply( this, arguments );

    this.config = {
      moduleconfig: this.config.moduleconfig
    };

  }

  /**
   * Handle requests
   * @param ctx
   * @param next
   */
  public handle( ctx, next ) {

    // session um einen client zu erzeugen
    if ( !ctx.debug ) {
      if ( ctx.session ) {
        this.dpd = internalClient.build( (process as any).server, ctx.session, null, ctx );
      } else {
        this.dpd = internalClient.build( (process as any).server, null, null, ctx );
      }
    }

    if ( ctx.method === 'GET' && ctx.query ) {
      const contentController: ContentController = new ContentController( this.dpd );
      contentController.handleContext( ctx );
    }
  }
}

module.exports = CmsContent;
