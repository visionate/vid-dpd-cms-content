

import { CmsContent } from './index';

const dpd = require( 'dpd-js-sdk' )( 'http://sonepar-il.dyndns.org:2503' );
dpd.pages = dpd( '/pages' );
dpd.contents = dpd( '/contents' );
dpd.projects = dpd( '/projects' );
dpd.files = dpd( '/files' );
dpd.i18n = dpd( '/i18n' );
dpd.templates = dpd( '/templates' );
dpd.soneparvisits = dpd( '/sonepar/visits' );
dpd.soneparvisitors = dpd( '/sonepar/visitors' );
dpd.soneparcontent = dpd( '/sonepar/content' );

const test = new CmsContent( null );

test.dpd = dpd;

test.handle( {
  done: ( error, res ) => {
    if ( error ) {
      console.log( 'done:error', error );
    } else {
      console.log( 'done:res', res );
    }
  },
  query: {
    visitorID_: '32c8649f76e27b6e',
    firstQuestion1_: 'true',
    questionID1_: '',
    answerID_: '34949d856da75ae2', // 'f5d17a0ce78a58d7', // 'c0a0ca035ac59b9d',
    contentID: '360e674a4e5b194a', // 'f5d17a0ce78a58d7', // 'c0a0ca035ac59b9d',
    lang: 'hu'
  },
  method: 'GET',
  debug: true
}, () => {

} );


