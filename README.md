# Readme

## SSH Login

    ssh 'admin@10.12.10.10'   
    admin
    Naovc$+3ZHp9q
   
    
## log into docker container

    # become root
    sudo su
    
    # log into container
    docker exec -t -i SIL-SERVER-API /bin/bash
    
    # change to project root
    cd /app/src/apiApp/
    
    # logs
    npm run logs
    
    # install
    npm install vid-dpd-cms-content

